#    Copyright 2020 - present Jonas Waeber
#
#    Licensed under the Apache License, Version 2.0 (the 'License');
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an 'AS IS' BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
import json

from init_client import world_id, client

if __name__ == '__main__':


    teyla = '7b6f34fc-38f2-467f-8353-417687ffbfa8'




    full_test_article = client.article.get(
        teyla,
        3
    )

    print(json.dumps(full_test_article, indent=4, sort_keys=True))